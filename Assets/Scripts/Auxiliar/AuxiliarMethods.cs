﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public static class AuxiliarMethods{

	public static IEnumerator WaitAndAction (float time, Action action){
		yield return new WaitForSeconds (time);
		action ();
	}

	public static float AngleBetweenPointsDegrees(Vector2 p1, Vector2 p2){
		float angle = Mathf.Atan2 (p2.y - p1.y, p2.x - p1.x)* Mathf.Rad2Deg;
		if (angle < 0)
			angle = 360 + angle;
		return angle;
	}

	public static float AngleBetweenPointsRadians(Vector2 p1, Vector2 p2){
		float angle = Mathf.Atan2 (p2.y - p1.y, p2.x - p1.x);
		return angle;
	}

	public static float AngleVector(Vector2 vect){
		return AngleBetweenPointsDegrees (Vector2.zero, vect);
	}

	public static float AngleVectorRadians(Vector2 vect){
		return AngleBetweenPointsRadians (Vector2.zero, vect);
	}

	public static Vector2 AngleDegreesToNormalizedVector(float angle){
		float angleInRadians = (Mathf.PI * angle) / 180;
		Vector2 vectorAngle = new Vector2 (Mathf.Cos (angleInRadians), Mathf.Sin (angleInRadians));
		return vectorAngle;
	}

	public static Vector2 V3toV2(Vector3 v3){
		return v3;
	}

	public static Vector2 DeltaDistanceVectorMoveTowards(Vector2 current, Vector2 target, float  maxDeltaDistance){
		Vector2 delta = target - current;
		if (delta.magnitude > maxDeltaDistance)
			return delta.normalized * maxDeltaDistance;
		else
			return delta;
	}

	public static bool isLayerInLayerMask(int layerID, int layerMask){
		if (layerMask == (layerMask | (1 << layerID))) {
			return true;
		} else {
			return false;
		}
	}

	public static void SetImageAlpha(Image targetImage, float alpha){
		Color tempColor = targetImage.color;
		tempColor.a = alpha;
		targetImage.color = tempColor;
	}
	// Collisions and overlap

	public static bool OverlapedBoxesColliders2D(BoxCollider2D collider1, BoxCollider2D collider2){
		// Max
		if (collider1.bounds.min.x > collider2.bounds.max.x)
			return false;
		if (collider1.bounds.min.y > collider2.bounds.max.y)
			return false;
		// Min
		if (collider1.bounds.max.x < collider2.bounds.min.x)
			return false;
		if (collider1.bounds.max.y < collider2.bounds.min.y)
			return false;
		//
		return true;
	}
		
	public static bool OvelapedRects(Vector2 rect1Center, Vector2 rect1Size, Vector2 rect2Center, Vector2 rect2Size){
		Vector2 rect1max = rect1Center + rect1Size/2;
		Vector2 rect1min = rect1Center - rect1Size/2;
		//
		Vector2 rect2max = rect2Center + rect2Size/2;
		Vector2 rect2min = rect2Center - rect2Size/2;
		// max
		if (rect1min.x > rect2max.x)
			return false;
		if (rect1min.y > rect2max.y)
			return false;
		// min
		if (rect1max.x < rect2min.x)
			return false;
		if (rect1max.y < rect2min.y)
			return false;
		//
		return true;
	}

	public static bool OverlapedCircles(Vector2 circle1Center, float circle1Radius, Vector2 circle2Center, float circle2Radius){
		float distance = (circle2Center - circle1Center).magnitude;
		return distance < (circle1Radius + circle2Radius);
	}

	public static bool OverlapedRectAndCircle( Vector2 rectCenter, Vector2 rectSize, Vector2 circleCenter, float circleRadius){
		Vector2 rectMax = rectCenter + rectSize / 2;
		Vector2 rectMin = rectCenter + rectSize / 2;
		//
		Vector2 closestRectPoint = new Vector2(Mathf.Clamp (circleCenter.x, rectMin.x, rectMax.x),Mathf.Clamp (circleCenter.y, rectMin.y, rectMax.y));

		Vector2 distance = circleCenter - closestRectPoint;

		return distance.magnitude < circleRadius;

	}

}
