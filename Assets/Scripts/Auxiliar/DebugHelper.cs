﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DebugHelper
{

    public static void GizmoDrawArrow(Vector2 pos, Vector2 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
    {
        Gizmos.DrawRay(pos, direction);

        Vector3 right =  Quaternion.Euler(0, 0, AuxiliarMethods.AngleVector(direction) + (180 + arrowHeadAngle)) * new Vector3(1, 0, 0);
        Vector3 left =  Quaternion.Euler(0, 0, AuxiliarMethods.AngleVector(direction) + (180 - arrowHeadAngle)) * new Vector3(1, 0, 0);
        Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
        Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
    }
}
