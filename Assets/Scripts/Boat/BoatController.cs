﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BoatController : MonoBehaviour {

    // Properties

    [SerializeField] private WaveGenerator waveGenerator;

    [SerializeField] [Range(0, 100)] private float boatCenterToFrontOffset = 2;
    [Header("Boyance")]
    [SerializeField] [Range(0, 100)] private float downSpeedScale = 10;
    [SerializeField] [Range(0, 100)] private float UpSpeedScale = 5;
    [SerializeField] [Range(0, 100)] private float stopSpeed = 1;
    [Space(10)]
    [SerializeField] [Range(0, 0.5f)] private float varianceY = 0.04f;
    [SerializeField] [Range(0, 1)] private float oscilationSpeedY = 0.3f;

    [Header("BoatAngle")]
    [SerializeField] [Range(0, 100)] private float angleDownSpeedScale = 10;
    [SerializeField] [Range(0, 100)] private float angleUpSpeedScale = 10;
    [SerializeField] [Range(0, 100)] private float angleStopSpeed = 40;
    [Space(10)]
    [SerializeField] [Range(0, 10f)] private float varianceAngle = 1f;
    [SerializeField] [Range(0, 1)] private float oscilationSpeedAngle = 0.2f;


    // States

    private DynamicOscilator boatYOscilator;
    private DynamicOscilator boatAngleOscilator;

    private SimpleOscilator boatYSimpleOscilator;
    private SimpleOscilator boatAngleSimpleOscilator;



    private float speedY;


    void Start () {
        InitializeProperties();
	}

    private void InitializeProperties()
    {
        boatYOscilator = new DynamicOscilator(UpSpeedScale, downSpeedScale, stopSpeed, transform.position.y);
        boatAngleOscilator = new DynamicOscilator(angleUpSpeedScale, angleDownSpeedScale, angleStopSpeed, transform.eulerAngles.z, true);

        boatYSimpleOscilator = new SimpleOscilator(-varianceY, varianceY, oscilationSpeedY);
        boatAngleSimpleOscilator = new SimpleOscilator(-varianceAngle, varianceAngle, oscilationSpeedAngle);
    }
	
	void Update () {
        // Boat Boyance
        float newY = boatYOscilator.DoUpdate(waveGenerator.GetWaveHeigthAtPosition(transform.position.x), Time.deltaTime);
        float varianceY = boatYSimpleOscilator.GetNextValue();
        transform.position = new Vector3(transform.position.x, newY + varianceY, transform.position.z);
        // Boat Angle
        Vector2 v1 = new Vector2(transform.position.x, waveGenerator.GetWaveHeigthAtPosition(transform.position.x));
        float x2 = transform.position.x + boatCenterToFrontOffset;
        Vector2 v2 = new Vector2(x2, waveGenerator.GetWaveHeigthAtPosition(x2));
        float targetAngle = boatAngleOscilator.DoUpdate(AuxiliarMethods.AngleBetweenPointsDegrees(v1, v2), Time.deltaTime);
        float varianceAngle = boatAngleSimpleOscilator.GetNextValue();
        transform.eulerAngles = new Vector3(0, 0, targetAngle + varianceAngle);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, transform.position + (transform.right*boatCenterToFrontOffset));
        Handles.color = Color.magenta;
        Handles.DrawSolidDisc(transform.position + (transform.right * boatCenterToFrontOffset), Vector3.forward, 0.1f);
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (Application.isPlaying)
        {
            InitializeProperties();
            Debug.Log("Updating BoatController Properties at object: " + gameObject.name);
        }
    }
#endif

}
