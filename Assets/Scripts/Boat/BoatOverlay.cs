﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatOverlay : MonoBehaviour {

    [SerializeField] private SpriteRenderer overlayRenderer;
    [SerializeField] private float maxCameraDistanceToHide = 11f;
    [SerializeField] private float fadeSpeed = 1f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(DynamicGameCamera.Singleton != null)
        {
            if (DynamicGameCamera.Singleton.CameraDistance < maxCameraDistanceToHide)
            {
                //fade in
                if (overlayRenderer.color.a > 0)
                {
                    var c = overlayRenderer.color;
                    c.a = Mathf.Clamp01(c.a - fadeSpeed * Time.deltaTime);
                    overlayRenderer.color = c;
                }
            }
            else
            {
                if (overlayRenderer.color.a < 1)
                {
                    var c = overlayRenderer.color;
                    c.a = Mathf.Clamp01(c.a + fadeSpeed * Time.deltaTime);
                    overlayRenderer.color = c;
                }
            }
        }
	}
}
