﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Navigation2D;
public class PlayerCharacter : Character
{
    private static PlayerCharacter selectedCharacter = null;

    [HideInInspector][SerializeField]private NavigationAgent2D navigationAgent;

    [SerializeField] private AudioClip[] pirateVoices;
    [SerializeField] private GameObject characterSelectedIndicator;

    private Vector2 selectedIndicatorLocalPosition;

    private Animator characterAnimator;
    private SpriteRenderer spriteRend;
    private AudioSource audioSource;

    private SimpleOscilator indicatorOscilator;

    private void Awake()
    {
        characterAnimator = gameObject.GetComponent<Animator>();
        spriteRend = gameObject.GetComponent<SpriteRenderer>();
        audioSource = gameObject.GetComponent<AudioSource>();
        selectedIndicatorLocalPosition = characterSelectedIndicator.transform.localPosition;
        indicatorOscilator = new SimpleOscilator(-0.02f, 0.02f, 1f);
    }

    // Start is called before the first frame update
    void Start()
    {
        navigationAgent.SnapToNavigationMesh();
    }

    // Update is called once per frame
    void Update()
    {
        characterAnimator.SetBool("walking", navigationAgent.IsMoving);
        characterAnimator.SetBool("climbing", navigationAgent.IsClimbing);
        spriteRend.flipX = !navigationAgent.IsFacingRight;

        if (selectedCharacter == this)
        {
            characterSelectedIndicator.transform.localPosition = new Vector2(selectedIndicatorLocalPosition.x, selectedIndicatorLocalPosition.y + indicatorOscilator.GetNextValue());

            if (Input.GetMouseButtonDown(1))
            {
                if (navigationAgent.SetTarget(DynamicGameCamera.Singleton.MouseToWorldPosition(transform.position.z)))
                {
                   // audioSource.PlayOneShot(pirateVoices[Random.Range(0, pirateVoices.Length)]);
                    Debug.Log("Path found");
                }
                else
                {
                    Debug.LogWarning("failed to find path");
                }
            }
        }
    }

    public void SelectCharacter()
    {
        if(selectedCharacter != null)
        {
            selectedCharacter.OnUnselected();
        }
        selectedCharacter = this;
        OnSelected();
    }

    private void OnSelected()
    {
        Debug.Log("SelectedCharacter" + gameObject);
        audioSource.PlayOneShot(pirateVoices[Random.Range(0, pirateVoices.Length)]);
        characterSelectedIndicator.SetActive(true);
    }

    private void OnUnselected()
    {
        characterSelectedIndicator.SetActive(false);
    }

    private void OnMouseDown()
    {
        SelectCharacter();
    }

    //-------------------- EDITOR ---------------------------//

    private void OnValidate()
    {
        if(navigationAgent == null)
        {
            navigationAgent = gameObject.GetComponent<NavigationAgent2D>();
            if(navigationAgent == null)
            {
                navigationAgent = gameObject.AddComponent<NavigationAgent2D>();
            }
        }
    }
}
