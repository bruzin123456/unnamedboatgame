﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicGameCamera : MonoBehaviour {

    // Singleton behaviour
    private static DynamicGameCamera _singleton;
    public static DynamicGameCamera Singleton { get { return _singleton; } }

    //Config
    [Header("Target")]
    [SerializeField] private Transform targetTransform;
    [SerializeField] private Vector2 targetPosition;


    [Header("Config")]
    [SerializeField] [Range(0, 100)] private float cameraHeight = 5;
    [SerializeField] [Range(0, 100)] private float cameraMinSpeed = 0.2f;
    [SerializeField] [Range(0, 100)] private float cameraMaxSpeed = 45f;
    [SerializeField] [Range(1, 100)] private float cameraDistanceToMaxSpeed = 45f;

    [Header("Zooming")]
    [SerializeField] private bool allowCameraZooming = true;
    [SerializeField] [Range(0, 100)] private float minCameraDistance = 10;
    [SerializeField] [Range(0, 100)] private float maxCameraDistance = 15;
    [SerializeField] [Range(0, 100)] private float zoomScrollSpeed = 15;
    [SerializeField] [Range(0, 100)] private float cameraZoomMaxSpeed = 2;
    [SerializeField] [Range(0, 100)] private float cameraZoomMinSpeed = 0.1f;
    [SerializeField] [Range(0, 100)] private float cameraZoomDistanceToMaxSpeed = 0.1f;



    [Header("Offset")]
    [SerializeField] private bool enableMouseOffset;
    [SerializeField] [Range(0, 100)] private float maximumMouseOffset = 2f;
    [SerializeField] [RangeAttribute(0, 1f)] private float screenRatioToMaximumMouseOffset = 0.9f;
    [SerializeField] [RangeAttribute(0, 1f)] private float startMouseOffsetAtScreenRatio = 0.2f;

    // Some properties

    public float CameraDistance{ get { return Mathf.Abs(transform.position.z); } }


    // Other

    private Camera usedCamera;
    private Nullable<float> targetCameraDistance = null;

    void Awake()
    {
        _singleton = this;
    }
	
	void Update () {

        if (targetTransform != null)
        {
            this.targetPosition = targetTransform.position;
        }

        Vector2 currentPosition = transform.position;
        Vector2 targetPosition = this.targetPosition; // local scope for target position

        if (enableMouseOffset)
        {
            targetPosition += GetMouseOffset();
        }

        float zDistance = minCameraDistance;
        if (allowCameraZooming)
        {
            if (targetCameraDistance == null)
                targetCameraDistance = minCameraDistance;
            targetCameraDistance = targetCameraDistance.Value - Time.deltaTime * zoomScrollSpeed * Input.mouseScrollDelta.y;
            targetCameraDistance = Mathf.Clamp(targetCameraDistance.Value, minCameraDistance, maxCameraDistance);
            var zoomSpeed = Mathf.Lerp(cameraZoomMinSpeed, cameraZoomMaxSpeed, (Mathf.Abs(targetCameraDistance.Value + transform.position.z) / cameraZoomDistanceToMaxSpeed));
            zDistance = Mathf.MoveTowards(Mathf.Abs(transform.position.z), targetCameraDistance.Value, zoomSpeed*Time.deltaTime);
        }
        else
        {
            targetCameraDistance = null;
        }

        var delta = targetPosition - currentPosition;
        var speed = Mathf.Lerp(cameraMinSpeed, cameraMaxSpeed, delta.magnitude / cameraDistanceToMaxSpeed);
        var cameraMovement = AuxiliarMethods.DeltaDistanceVectorMoveTowards(currentPosition, targetPosition, speed * Time.deltaTime);
    



        transform.position = new Vector3(currentPosition.x + cameraMovement.x, currentPosition.y + cameraMovement.y, -zDistance);
	}

    private Vector2 GetMouseOffset()
    {
        var mousePositionOffset = new Vector2(
            Mathf.Clamp(((Input.mousePosition.x / Screen.width) - 0.5f) * 2f, -1, 1),
            Mathf.Clamp(((Input.mousePosition.y / Screen.height) - 0.5f) * 2f, -1, 1));

        float offsetMagnitude = mousePositionOffset.magnitude;

        if (offsetMagnitude < startMouseOffsetAtScreenRatio)
            return Vector2.zero;

        return mousePositionOffset.normalized * Mathf.Lerp(0,
                                                         maximumMouseOffset,
                                                         (offsetMagnitude - startMouseOffsetAtScreenRatio)/(screenRatioToMaximumMouseOffset - startMouseOffsetAtScreenRatio));
    }

    //Camera Data

    public struct CameraWorldPositionBounds
    {
        public CameraWorldPositionBounds(Vector2 min, Vector2 max)
        {
            this.min = min;
            this.max = max;
        }

        private Vector2 min;
        public Vector2 Min { get { return min; } }

        private Vector2 max;
        public Vector2 Max { get { return max; } }

        public float Left { get { return min.x;} }
        public float Right { get { return max.x; } }
        public float Top { get { return max.y; } }
        public float Bottom { get { return min.y; } }

        public float Length { get { return Math.Abs(max.x - min.x); } }
    }

    public CameraWorldPositionBounds GetCameraBounds(float zCoordinate)
    {
        float z = zCoordinate - transform.position.z;
        Vector2 max = usedCamera.ViewportToWorldPoint(new Vector3(1, 1, z));
        Vector2 min = usedCamera.ViewportToWorldPoint(new Vector3(0, 0, z));
        return new CameraWorldPositionBounds(min, max);
    }

    public Vector3 CameraToWorldPosition(Vector2 position, float z)
    {
        return Camera.main.ScreenToWorldPoint(new Vector3(position.x, position.y, z - transform.position.z));
    }

    public Vector3 MouseToWorldPosition(float z)
    {
        return CameraToWorldPosition(Input.mousePosition, z);
    }


    // Validation

    private void OnValidate()
    {
        //Validate Fields
        if (minCameraDistance > maxCameraDistance)
            maxCameraDistance = minCameraDistance;

        if (cameraMinSpeed > cameraMaxSpeed)
            cameraMaxSpeed = cameraMinSpeed;

        usedCamera = gameObject.GetComponent<Camera>();
        if(usedCamera == null)
        {
            usedCamera = gameObject.AddComponent<Camera>();
        }

        // Used to decide camera fieldOfView for locking camera distance and screen height size in world space
        //usedCamera.orthographic = false;
        //usedCamera.fieldOfView = 2.0f * Mathf.Atan(cameraHeight * 0.5f / cameraDistance) * Mathf.Rad2Deg;
    }



}
