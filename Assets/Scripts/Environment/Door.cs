﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Navigation2D;

public class Door : MonoBehaviour
{

    [SerializeField] private Sprite closedDoorSprite;
    [SerializeField] private Sprite openedDoorSprite;
    [SerializeField] private bool invertDoorDirection;
    [SerializeField] private DoorNavigationConnection2D doorNavigationConnection;
    [SerializeField] private float doorOpenDuration = 1;
    [SerializeField] private AudioClip doorOpenSound;


    private bool doorIsOpen;
    private SpriteRenderer spriteRend;
    private AudioSource audioSource;
    private Coroutine closeDoorCoroutine;

    void Awake()
    {
        spriteRend = gameObject.GetComponent<SpriteRenderer>();
        spriteRend.sprite = closedDoorSprite;
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    void Start()
    {
        if(doorNavigationConnection != null)
        {
            doorNavigationConnection.AddEnteredConnectionCallback(OnDoorConnectionEntered);
        }
    }



    private void OnDoorConnectionEntered(NavigationAgent2D navigationAgent) 
    {
        if (!doorIsOpen)
        {
            if(doorOpenSound != null)
            {
                audioSource.PlayOneShot(doorOpenSound, 0.6f);
            }
            doorIsOpen = true;
            spriteRend.sprite = openedDoorSprite;
            spriteRend.flipX = (navigationAgent.transform.position.x < transform.position.x);
            if (invertDoorDirection)
            {
                spriteRend.flipX = !spriteRend.flipX;
            }
        }
        else
        {
            if(closeDoorCoroutine != null)
            {
                StopCoroutine(closeDoorCoroutine);
            }
        }
        closeDoorCoroutine = StartCoroutine(AuxiliarMethods.WaitAndAction(doorOpenDuration, CloseDoor));
    }

    private void CloseDoor()
    {
        closeDoorCoroutine = null;
        doorIsOpen = false;
        spriteRend.sprite = closedDoorSprite;
    }

    // Editor

    private void OnValidate()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        if(audioSource == null)
        {
            audioSource = gameObject.GetComponent<AudioSource>();
        }
    }
}
