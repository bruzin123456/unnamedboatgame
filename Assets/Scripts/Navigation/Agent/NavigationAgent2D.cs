﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Navigation2D {

    public class NavigationAgent2D : MonoBehaviour
    {

        [SerializeField] private NavigationMesh2D navigationMesh;
        public NavigationMesh2D NavigationMesh { get { return navigationMesh; } set { navigationMesh = value; } }

        [SerializeField] private float movementSpeed = 5f;
        public float MovementSpeed { get { return movementSpeed;} set {movementSpeed = value;}}

        private NavigationLocation target;
        public NavigationLocation Target { get { return target; } }

        private NavigationLocation currentNavigationLocation;

        private NavigationPath navigationPath;
        private LinkedList<MovementOrder> movementOrders = new LinkedList<MovementOrder>();
        private bool facingRight;
        private bool climbing;


        private MovementOrder CurrentMovementOrder {  get { return movementOrders.Count > 0 ? movementOrders.First.Value : null; } }
        public bool IsMoving { get { return CurrentMovementOrder != null; } }
        public bool IsMovingTroughConnection { get { return IsMoving ? CurrentMovementOrder.IsConnection : false; } }

        public bool IsFacingRight {  get { return facingRight; } }
        public bool IsClimbing {  get { return climbing; } }

        

        // Update is called once per frame
        void Update()
        {
            if (IsMoving)
            {
                MovementOrder mOrder = CurrentMovementOrder;
          

                float speedScale = 1;
                if(mOrder.connection != null)
                {
                    speedScale *= mOrder.connection.ConnectionMovementSpeedScale;
                }
                facingRight = mOrder.NavMeshLocalTargetPosition.x > currentNavigationLocation.nLocalPosition.x;
                currentNavigationLocation.nLocalPosition = Vector2.MoveTowards(currentNavigationLocation.nLocalPosition, mOrder.NavMeshLocalTargetPosition, movementSpeed * Time.deltaTime * speedScale);
                MoveToNavigationLocation(currentNavigationLocation);
                if (currentNavigationLocation.nLocalPosition == mOrder.NavMeshLocalTargetPosition)
                {
                  currentNavigationLocation.navigationArea = mOrder.targetNavigationArea;
                  NextOrder();
                }
            }
        }

        private MovementOrder NextOrder()
        {
            movementOrders.RemoveFirst();
            climbing = false;
            MovementOrder newOrder = CurrentMovementOrder;
            if (newOrder == null)
            {
                ProcessStep();
                newOrder = CurrentMovementOrder;
            }
            if (newOrder != null)
            {
                if (newOrder.IsConnection)
                {
                    newOrder.connection.OnEnteredConnection(this);
                    if(newOrder.connection is LadderNavigationConnection2D)
                    {
                        climbing = true;
                    }
                }
            }
            return newOrder;
        }

        private void ProcessStep()
        {
          NavigationPath.NavigationStep navigationStep = (navigationPath != null && navigationPath.Steps.Count > 0) ? navigationPath.Steps.First.Value : null;
          if(navigationStep == null)
            {
                return;
            }
            navigationPath.Steps.RemoveFirst();
            if (navigationStep.navigationConnection != null)
            {
                MovementOrder goToConnectionOrder = new MovementOrder();
                goToConnectionOrder.targetNavigationArea = currentNavigationLocation.navigationArea;
                goToConnectionOrder.NavMeshLocalTargetPosition = navigationStep.navigationConnection.GetConnectionPositionAtArea(currentNavigationLocation.navigationArea);
                movementOrders.AddLast(goToConnectionOrder);

                MovementOrder passThroughConnectionOrder = new MovementOrder();
                passThroughConnectionOrder.targetNavigationArea = navigationStep.navigationConnection.GetOtherEndArea(currentNavigationLocation.navigationArea);
                passThroughConnectionOrder.NavMeshLocalTargetPosition = navigationStep.navigationConnection.GetConnectionPositionAtArea(passThroughConnectionOrder.targetNavigationArea);
                passThroughConnectionOrder.connection = navigationStep.navigationConnection;
                movementOrders.AddLast(passThroughConnectionOrder);
            }
            else
            {
                MovementOrder goToLocationOrder = new MovementOrder();
                goToLocationOrder.targetNavigationArea = navigationStep.targetNavigationLocation.navigationArea;
                goToLocationOrder.NavMeshLocalTargetPosition = navigationStep.targetNavigationLocation.nLocalPosition;
                movementOrders.AddLast(goToLocationOrder);
            }
        }

        //

        public void SnapToNavigationMesh()
        {
            NavigationLocation nLocation = FindCurrentNavigationLocation();

            if(nLocation != null)
            {
                nLocation.nLocalPosition = new Vector2(nLocation.nLocalPosition.x, nLocation.navigationArea.NavMeshLocationPosition.y);
                MoveToNavigationLocation(nLocation);
            }

        }

        public void MoveToNavigationLocation(NavigationLocation navigationLocation)
        {
            if(navigationLocation == null)
            {
                return;
            }

            transform.position = navigationMesh.transform.TransformPoint(navigationLocation.nLocalPosition);

            currentNavigationLocation = navigationLocation;

        }

        private NavigationArea2D FindCurrentNavigationArea()
        {
           NavigationLocation navLocation = FindCurrentNavigationLocation();
            if (navLocation != null)
                return navLocation.navigationArea;
            return null;
        }

        private NavigationLocation FindCurrentNavigationLocation()
        {
            return NavigationMesh.GetNavigationLocationFromWorldPosition(transform.position);
        }

        public bool SetTarget(NavigationLocation targetNavigationLocation)
        {
            Stop();
            target = targetNavigationLocation;

            if(target == null)
            {
                return false;
            }
            navigationPath = NavigationMesh.FindPath(currentNavigationLocation, targetNavigationLocation);
            ProcessStep();
            return navigationPath != null && navigationPath.Steps.Count > 0;

        }

        public bool SetTarget(Vector2 targetPosition)
        {
            NavigationLocation navLocation = NavigationMesh.GetNavigationLocationFromWorldPosition(targetPosition);
            if(navLocation != null)
            {
                navLocation.nLocalPosition = new Vector2(navLocation.nLocalPosition.x, navLocation.navigationArea.NavMeshLocationPosition.y);
                return SetTarget(navLocation);
            }
            return false;
        }

        public void Stop()
        {
            target = null;
            navigationPath = null;
            movementOrders.Clear();
        }
    }

    class MovementOrder
    {
        public Vector2 NavMeshLocalTargetPosition;
        public NavigationArea2D targetNavigationArea;
        public NavigationConnection2D connection;

        public bool IsConnection { get { return connection != null; } }
    }

    [System.Serializable]
    public class NavigationAgent2DEvent : UnityEvent<NavigationAgent2D>
    {
    }

    //

}
