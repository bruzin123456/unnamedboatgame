﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Navigation2D
{

    public class NavigationArea2D : NavigationComponent2D
    {

        public static Color DebugAreaColor = Color.cyan;
        public static Color DebugWalkableColor = Color.magenta;

        //Inspector
        [Range(0.5f, 10f)] [SerializeField] private float _extents = 2f;
        [Range(0.5f, 10f)] [SerializeField] private float _height = 1f;

        //States
        [HideInInspector] [SerializeField] private List<NavigationConnection2D> navigationConnections = new List<NavigationConnection2D>();

        // Properties
        public float Extents { get { return _extents; } }
        public float Height { get { return _height; } }


        public Vector2 Center { get { return transform.position + (transform.up * (Height / 2)); } }
        public float Rotation { get { return transform.eulerAngles.z; } }

        public Vector2 UnrotatedMin { get { return new Vector2(transform.position.x - Extents, transform.position.y); } }
        public Vector2 UnrotatedMax { get { return new Vector2(transform.position.x + Extents, transform.position.y + Height); } }

        public Vector2 BottomLeft { get { return transform.position - (transform.right * (Extents)); } }
        public Vector2 BottomRight { get { return transform.position + (transform.right * (Extents)); } }
        public Vector2 TopLeft { get { return transform.position - (transform.right * (Extents)) + (transform.up * Height); } }
        public Vector2 TopRight { get { return transform.position + (transform.right * (Extents)) + (transform.up * Height); } }

        public Vector2 NavMeshLocationPosition { get {return NavMesh.transform.InverseTransformPoint(transform.position); } }
        public Vector2 NavMeshLocalBottomLeft { get { return NavMesh.transform.InverseTransformPoint(BottomLeft); } }
        public Vector2 NavMeshLocalBottomRight { get { return NavMesh.transform.InverseTransformPoint(BottomRight); } }
        public Vector2 NavMeshLocalTopLeft { get { return NavMesh.transform.InverseTransformPoint(TopLeft); } }
        public Vector2 NavMeshLocalTopRight { get { return NavMesh.transform.InverseTransformPoint(TopRight); } }

        public List<NavigationConnection2D> NavigationConnections { get { return navigationConnections; } }




        // Setters

        public void SetExtents(float x)
        {
            if (x < 0)
                x = 0;
            _extents = x;
        }

        public void SetHeight(float x)
        {
            if (x < 0)
                x = 0;
            _height = x;
        }

        public void SetBottomCenter(Vector2 x)
        {
            transform.position = x;
        }

        // Auxiliar

        public bool IsWorldPositionInside(Vector3 pos)
        {
            // Rotation point
            var deltaPos = pos - transform.position;
            deltaPos = Quaternion.Euler(0, 0, -Rotation) * deltaPos;
            //Check inside bounds
            if (deltaPos.x < -Extents || deltaPos.y < 0 || deltaPos.x > Extents || deltaPos.y > Height)
                return false;

            return true;
        }

        public System.Nullable<Vector2> WorldPositionToNavigationMeshPosition(Vector3 pos)
        {
            // Rotation point
            var deltaPos = pos - transform.position;
            deltaPos = Quaternion.Euler(0, 0, -Rotation) * deltaPos;
            if (deltaPos.x < -Extents || deltaPos.y < 0 || deltaPos.x > Extents || deltaPos.y > Height)
                return null;
            Vector2 nLocalAreaPosition = NavMesh.transform.InverseTransformPoint(transform.position);
            return new Vector2(nLocalAreaPosition.x + deltaPos.x, nLocalAreaPosition.y + deltaPos.y);

        }




        //Editor

        override protected void OnValidate()
        {
            base.OnValidate();
            if (Mathf.Abs(Rotation) > 90)
            {
                Debug.LogError("NavigationArea2D can't have rotations grater than 90 degrees");
            }
        }


        public override void DrawDebug()
        {
            base.DrawDebug();
            Gizmos.color = DebugAreaColor;
            Gizmos.DrawLine(BottomLeft, TopLeft);
            Gizmos.DrawLine(TopLeft, TopRight);
            Gizmos.DrawLine(TopRight, BottomRight);
            Gizmos.color = DebugWalkableColor;
            Gizmos.DrawLine(BottomRight, BottomLeft);
        }
    }

}
