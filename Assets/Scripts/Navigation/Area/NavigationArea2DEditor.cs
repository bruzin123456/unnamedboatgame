﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Navigation2D
{

    [CustomEditor(typeof(NavigationArea2D))]
    public class NavigationArea2DEditor : NavigationComponent2DEditor
    {

        public bool edit;

        float snap = 0.1f;
        float handleSize = 0.1f;


        void OnEnable()
        {
            Tools.hidden = true;
        }

        void OnDisable()
        {
            Tools.hidden = false;
        }


        private void OnSceneGUI()
        {
            var t = (target as NavigationArea2D);
            if (edit)
            {
                Vector3 leftInitPosition = (t.BottomLeft + t.TopLeft) / 2;
                Vector3 rightInitPosition = (t.BottomRight + t.TopRight) / 2;
                Vector3 topInitPosition = (t.TopLeft + t.TopRight) / 2;
                Vector3 bottomInitPosition = t.transform.position;
                Tools.hidden = true;
                Handles.color = NavigationArea2D.DebugAreaColor;
                Vector3 HandleRightPosition = Handles.Slider(rightInitPosition, t.transform.right, handleSize, Handles.DotHandleCap, snap);
                Vector3 HandleLeftPosition = Handles.Slider(leftInitPosition, t.transform.right, handleSize, Handles.DotHandleCap, snap);
                Vector3 HandleUpPosition = Handles.Slider(topInitPosition, t.transform.up, handleSize, Handles.DotHandleCap, snap);
                Handles.color = NavigationArea2D.DebugWalkableColor;
                Vector3 HandleDownPosition = Handles.Slider(bottomInitPosition, t.transform.up, handleSize, Handles.DotHandleCap, snap);
                //
                t.SetExtents((HandleRightPosition - HandleLeftPosition).magnitude / 2);
                t.SetHeight(((HandleUpPosition - HandleDownPosition)).magnitude);

                if (HandleLeftPosition != leftInitPosition || HandleRightPosition != rightInitPosition)
                {
                    t.SetBottomCenter(((HandleRightPosition + HandleLeftPosition) / 2) - (t.transform.up * t.Height / 2));
                }
                else
                {
                    t.SetBottomCenter(HandleDownPosition);
                }



            }
            else
            {
                Tools.hidden = false;
            }
        }

        public override void OnInspectorGUI()
        {


            if (edit != EditorGUILayout.Toggle("EditArea", edit))
            {
                edit = !edit;
                EditorUtility.SetDirty(target);
            }

            base.OnInspectorGUI();
        }


    }

}
