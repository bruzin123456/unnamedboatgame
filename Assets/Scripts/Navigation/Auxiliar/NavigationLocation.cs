﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Navigation2D
{
    public class NavigationLocation
    {
        public NavigationLocation(NavigationArea2D navigationArea, Vector2 nLocalPosition)
        {
            this.navigationArea = navigationArea;
            this.nLocalPosition = nLocalPosition;
        }
        public NavigationArea2D navigationArea;
        public Vector2 nLocalPosition;
    }
}
