﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Navigation2D
{

    public class NavigationPath
    {
        public class NavigationStep
        {
            public NavigationStep(NavigationLocation targetNavigationLocation)
            {
                this.targetNavigationLocation = targetNavigationLocation;
            }
            public NavigationStep(NavigationConnection2D navigationConnection)
            {
                this.navigationConnection = navigationConnection;
            }

            public NavigationLocation targetNavigationLocation;
            public NavigationConnection2D navigationConnection; // null if same room movement
        }

        private LinkedList<NavigationStep> steps = new LinkedList<NavigationStep>();

        public LinkedList<NavigationStep> Steps { get { return steps; } }
    }
}
