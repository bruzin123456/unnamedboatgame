﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Navigation2D
{

    public class DoorNavigationConnection2D : NavigationConnection2D
    {

        public override Vector2 GetConnectionPositionAtArea(NavigationArea2D nArea)
        {
            if (!isValidConnection())
            {
                throw new System.Exception("This connection is Invalid");
            }

            if (nArea != area1 && nArea != area2)
            {
                throw new System.Exception("this connection doesn't contain this area");
            }

            Vector3 nLocalArea1Position = NavMesh.transform.InverseTransformDirection(area1.transform.position);
            Vector3 nLocalArea2Position = NavMesh.transform.InverseTransformDirection(area2.transform.position);


            if (nArea == area1)
            {
                if (nLocalArea1Position.x < nLocalArea2Position.x)
                {
                    return area1.NavMeshLocalBottomRight;
                }
                else
                {
                    return area1.NavMeshLocalBottomLeft;
                }
            }
            else
            {
                if (nLocalArea2Position.x < nLocalArea1Position.x)
                {
                    return area2.NavMeshLocalBottomRight;
                }
                else
                {
                    return area2.NavMeshLocalBottomLeft;
                }
            }

        }


        protected override void OnValidate()
        {
            base.OnValidate();
            if (Area1 != null && Area2 != null)
            {
                if (Area1.transform.position.y != Area2.transform.position.y)
                {
                    Debug.LogWarning("Areas Not at the same Y");
                }
            }
        }

        public override void DrawDebug()
        {
            base.DrawDebug();
            Gizmos.color = Color.green;
            DebugHelper.GizmoDrawArrow(transform.position, transform.right * 0.5f);
            DebugHelper.GizmoDrawArrow(transform.position, transform.right * -0.5f);
        }
    }
}
