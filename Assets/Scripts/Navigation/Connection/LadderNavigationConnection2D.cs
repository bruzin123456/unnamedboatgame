﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Navigation2D
{

    public class LadderNavigationConnection2D : NavigationConnection2D
    {

        public override Vector2 GetConnectionPositionAtArea(NavigationArea2D nArea)
        {
            if (!isValidConnection())
            {
                throw new System.Exception("This connection is Invalid");
            }


            if (nArea != area1 && nArea != area2)
            {
                throw new System.Exception("this connection doesn't contain this area");
            }

            Vector3 nLocalAreaPosition = NavMesh.transform.InverseTransformPoint(nArea.transform.position);
            Vector3 nLocalConnectionPosition = NavMesh.transform.InverseTransformPoint(transform.position);

            return new Vector2(nLocalConnectionPosition.x, nLocalAreaPosition.y);
        }

        protected override void OnValidate()
        {
            base.OnValidate();
        }

        public override void DrawDebug()
        {
            base.DrawDebug();
            if (isValidConnection())
            {
                Gizmos.color = Color.green;
                DebugHelper.GizmoDrawArrow(transform.position, Quaternion.Euler(0, 0, transform.eulerAngles.z) * new Vector3(0, (NavMesh.transform.InverseTransformPoint(area1.transform.position).y - NavMesh.transform.InverseTransformPoint(transform.position).y)));
                DebugHelper.GizmoDrawArrow(transform.position, Quaternion.Euler(0, 0, transform.eulerAngles.z) * new Vector3(0, (NavMesh.transform.InverseTransformPoint(area2.transform.position).y - NavMesh.transform.InverseTransformPoint(transform.position).y)));
            }
        }


    }

}
