﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Navigation2D
{

    public abstract class NavigationConnection2D : NavigationComponent2D
    {

        [SerializeField] protected NavigationArea2D area1;
        [SerializeField] protected NavigationArea2D area2;
        [SerializeField][Range(0, 2)] protected float connectionMovementSpeedScale = 1;

        public NavigationArea2D Area1 { get { return area1; } }
        public NavigationArea2D Area2 { get { return area2; } }
        public float ConnectionMovementSpeedScale {  get { return connectionMovementSpeedScale; } }

        private NavigationAgent2DEvent enteredConnectionEvent;

        public abstract Vector2 GetConnectionPositionAtArea(NavigationArea2D nArea);

        public NavigationArea2D GetOtherEndArea(NavigationArea2D nArea)
        {
            if (nArea == null)
                return null;

            if (area1 == nArea)
                return area2;

            if (area2 == nArea)
                return area1;

            return null;
        }

        public virtual bool isValidConnection()
        {
            return area1 != null && area2 != null;
        }

        public virtual void OnEnteredConnection(NavigationAgent2D navigationAgent)
        {
            if(enteredConnectionEvent != null)
            {
                enteredConnectionEvent.Invoke(navigationAgent);
            }
        }

        public virtual void AddEnteredConnectionCallback(UnityAction<NavigationAgent2D> callback)
        {
            if(enteredConnectionEvent == null)
            {
                enteredConnectionEvent = new NavigationAgent2DEvent();
            }
            enteredConnectionEvent.AddListener(callback);
        }

        //Editor

        public void DrawDebugConnection()
        {

        }

        override protected void OnValidate()
        {
            base.OnValidate();
            if (area1 == null || area2 == null)
            {
                Debug.LogWarning("Missing  navigation area");
            }
            else if (NavMesh != null)
            {
                if (!NavMesh.ContainsNavigationArea(area1))
                {
                    Debug.LogWarning("Area1 is not inside this connection nav mesh");
                }

                if (!NavMesh.ContainsNavigationArea(area2))
                {
                    Debug.LogWarning("Area2 is not inside this connection nav mesh");
                }
            }
        }

    }

}
