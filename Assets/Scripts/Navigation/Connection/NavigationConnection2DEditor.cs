﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Navigation2D
{
    [CustomEditor(typeof(NavigationConnection2D), true)]
    public class NavigationConnection2DEditor : NavigationComponent2DEditor
    {

    }

}
