﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Navigation2D
{

    public abstract class NavigationComponent2D : MonoBehaviour
    {
        [HideInInspector] [SerializeField] private NavigationMesh2D _navMesh = null;
        public NavigationMesh2D NavMesh { get { return _navMesh; } set { _navMesh = value; } }


        protected virtual void OnValidate()
        {
            NavigationMesh2D nMesh = GetComponentInParent<NavigationMesh2D>();
            if (nMesh != null)
            {
                nMesh.GenMesh();
            }
        }

        protected virtual void OnDrawGizmos()
        {
            if (NavMesh != null && Selection.Contains(NavMesh.gameObject))
            {
                DrawDebug();
            }
            else if (Selection.Contains(gameObject))
            {
                if (NavMesh != null && Selection.gameObjects.Length == 1)
                {
                    NavMesh.DrawMeshDebug();
                }
                else
                {
                    DrawDebug();
                }
            }
        }

        public virtual void DrawDebug()
        {

        }


    }

}
