﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace Navigation2D
{
    [CustomEditor(typeof(NavigationComponent2D), true)]
    public class NavigationComponent2DEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            if ((target as NavigationComponent2D).NavMesh == null)
            {
                GUILayout.Space(20);
                EditorGUILayout.HelpBox("Not Attached to any NavigationMesh2D", MessageType.Warning);
            }

            GUILayout.Space(20);
            DrawDefaultInspector();
        }
    }

}
