﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Navigation2D
{

    public class NavigationMesh2D : MonoBehaviour
    {

        [HideInInspector] [SerializeField] private NavigationArea2D[] navigationAreasList;
        [HideInInspector] [SerializeField] private NavigationConnection2D[] navigationConnectionsList;



        public NavigationArea2D[] NavigationAreasList { get { return navigationAreasList; } }
        public NavigationConnection2D[] NavigationConnectionsList { get { return navigationConnectionsList; } }

        public void GenMesh()
        {
            // Areas
            navigationAreasList = gameObject.GetComponentsInChildren<NavigationArea2D>();
            if (navigationAreasList == null || navigationAreasList.Length == 0)
            {
                Debug.LogError("NavigationMesh2D No 2D Area found");
            }
            else
            {
                for (int i = 0; i < navigationAreasList.Length; i++)
                {
                    NavigationArea2D navArea = navigationAreasList[i];
                    navArea.NavMesh = this;
                    navArea.NavigationConnections.Clear();
                }
            }
            //Connections
            navigationConnectionsList = gameObject.GetComponentsInChildren<NavigationConnection2D>();
            if (navigationConnectionsList == null || navigationConnectionsList.Length == 0)
            {
                Debug.LogError("NavigationMesh2D No 2d connection found");
            }
            else
            {
                for (int i = 0; i < navigationConnectionsList.Length; i++)
                {
                    NavigationConnection2D navConnection = navigationConnectionsList[i];
                    navConnection.NavMesh = this;
                    if (navConnection.isValidConnection())
                    {
                        navConnection.Area1.NavigationConnections.Add(navConnection);
                        navConnection.Area2.NavigationConnections.Add(navConnection);
                    }
                    else
                    {
                        Debug.LogWarning("Invalid Connection: " + navConnection);
                    }

                }
            }
        }

        public bool ContainsNavigationArea(NavigationArea2D nArea)
        {
            if (navigationAreasList != null)
            {
                return System.Array.IndexOf(navigationAreasList, nArea) > -1;
            }
            return false;
        }

        public bool ContainsNavigationConnection(NavigationConnection2D nConnection)
        {
            if (navigationConnectionsList != null)
            {
                return System.Array.IndexOf(navigationConnectionsList, nConnection) > -1;
            }
            return false;
        }

        // --------------------------------------- PATH FINDING ------------------------------------ //

        public NavigationPath FindPath(NavigationLocation from, NavigationLocation to)
        {
            if (from == null || to == null)
            {
                Debug.LogWarning("Find Path null fields");
                return null;
            }
            if (from.navigationArea.NavMesh == null || from.navigationArea.NavMesh != to.navigationArea.NavMesh)
            {
                Debug.LogWarning("Find Path, Navigation areas from different navMeshes");
                return null;
            }
            NavigationPath navPath = new NavigationPath();

            if (from.navigationArea == to.navigationArea)
            {
                navPath.Steps.AddFirst(new NavigationPath.NavigationStep(to));
                return navPath;
            }
            //TODO this was done poorly optimize later
            List<NavigationArea2D> exploredAreas = new List<NavigationArea2D>();
            FindPathDepthSearch(from.navigationArea, to, exploredAreas, navPath);
            return navPath;
        }

        private bool FindPathDepthSearch(NavigationArea2D root, NavigationLocation target, List<NavigationArea2D> exploredAreas, NavigationPath navPath)
        {
            if (!exploredAreas.Contains(root))
            {
                exploredAreas.Add(root);
            }

            if(root == target.navigationArea)
            {
                navPath.Steps.AddFirst(new NavigationPath.NavigationStep(target));
                return true;
            }

            for (int i = 0; i < root.NavigationConnections.Count; i++)
            {
                NavigationConnection2D nConnection = root.NavigationConnections[i];
                NavigationArea2D nArea = root.NavigationConnections[i].GetOtherEndArea(root);
                if (!exploredAreas.Contains(nArea))
                {
                    if (FindPathDepthSearch(nArea, target, exploredAreas, navPath))
                    {
                        navPath.Steps.AddFirst(new NavigationPath.NavigationStep(nConnection));
                        return true;
                    }
                }
            }
            return false;
        }

        public NavigationLocation GetNavigationLocationFromWorldPosition(Vector3 p)
        {
            for (int i = 0; i < navigationAreasList.Length; i++)
            {
                NavigationArea2D nArea = navigationAreasList[i];
                var nLocalNavigationPosition = nArea.WorldPositionToNavigationMeshPosition(p);
                if (nLocalNavigationPosition != null)
                {
                    return new NavigationLocation(nArea, nLocalNavigationPosition.Value);
                }
            }
            return null;
        }

        //--------------------------------------- Editor --------------------------------------------//
        private void OnValidate()
        {
            GenMesh();
        }



        public void DrawMeshDebug()
        {
            if (navigationAreasList != null)
            {
                for (int i = 0; i < navigationAreasList.Length; i++)
                {
                    navigationAreasList[i].DrawDebug();
                }
            }

            if (navigationConnectionsList != null)
            {
                for (int i = 0; i < navigationConnectionsList.Length; i++)
                {
                    navigationConnectionsList[i].DrawDebug();
                }
            }
        }
    }

}
