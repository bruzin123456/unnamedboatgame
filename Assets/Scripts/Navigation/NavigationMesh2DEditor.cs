﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace Navigation2D
{
    [CustomEditor(typeof(NavigationMesh2D))]
    public class NavigationMesh2DEditor : Editor
    {

        private bool showNavigationAreas = false;
        private bool showNavigationConnections = false;

        public override void OnInspectorGUI()
        {
            NavigationMesh2D t = (NavigationMesh2D)target;

            if (GUILayout.Button("GenMesh"))
            {
                t.GenMesh();
                EditorUtility.SetDirty(t);
                return;
            }

            NavigationArea2D[] navigationAreas = t.NavigationAreasList;
            NavigationConnection2D[] navigationConnections = t.NavigationConnectionsList;

            if (navigationAreas != null && navigationAreas.Length > 0)
            {
                showNavigationAreas = EditorGUILayout.Foldout(showNavigationAreas, "Navigation Areas (" + navigationAreas.Length + ")");
                if (showNavigationAreas)
                {
                    for (int i = 0; i < navigationAreas.Length; i++)
                    {
                        EditorGUILayout.LabelField(i + "- " + navigationAreas[i].gameObject.name);
                    }
                }
            }
            else
            {
                EditorGUILayout.HelpBox("No Navigation Area Available", MessageType.Warning);
            }

            if (navigationConnections != null && navigationConnections.Length > 0)
            {
                showNavigationConnections = EditorGUILayout.Foldout(showNavigationConnections, "Navigation Connections (" + navigationConnections.Length + ")");
                if (showNavigationConnections)
                {
                    for (int i = 0; i < navigationConnections.Length; i++)
                    {
                        EditorGUILayout.LabelField(i + "- " + navigationConnections[i].gameObject.name);
                    }
                }
            }
            else
            {
                EditorGUILayout.HelpBox("No Navigation Connection Available", MessageType.Warning);
            }

            GUILayout.Space(30);
            DrawDefaultInspector();
        }

    }

}
