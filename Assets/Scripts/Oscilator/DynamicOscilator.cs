﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicOscilator {

    private float currentValue;
    private float speed;

    private float upScale;
    private float downScale;
    private float stopScale;

    private bool isAngle;

    public DynamicOscilator(float upScale, float downScale, float stopScale, float currentValue = 0, bool isAngle = false)
    {
        this.upScale = upScale;
        this.downScale = downScale;
        this.stopScale = stopScale;
        this.currentValue = currentValue;
        this.isAngle = isAngle;
    }
    public void SetUpScale(float val)
    {
        upScale = val;
    }

    public void SetDownScale(float val)
    {
        downScale = val;
    }

    public void SetGlobalScale(float val)
    {
        upScale = val;
        downScale = val;
    }

    public void SetStopScale(float val)
    {
        stopScale = val;
    }

    public void SetCurrentValue(float val)
    {
        currentValue = val;
    }

    public float DoUpdate(float targetValue, float timeStep)
    {
        float delta = (!isAngle) ? (targetValue - currentValue) : Mathf.DeltaAngle(currentValue, targetValue);
        if (delta > 0)
        {
            if (speed < 0)
            {
                speed += stopScale * Time.deltaTime * Mathf.Abs(speed);
                if (speed > 0)
                    speed = 0;
            }
            speed += delta * upScale * Time.deltaTime;
        }
        else
        {
            if (speed > 0)
            {
                speed -= stopScale * Time.deltaTime * Mathf.Abs(speed);
                if (speed < 0)
                    speed = 0;
            }
            speed += delta * downScale * Time.deltaTime;

        }
        currentValue = currentValue + (speed * timeStep);
        return currentValue;
    }

    
}
