﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OscilatorBase{

    public abstract float GetNextValue();

    public abstract float GetNextValue(float deltaTime);

    public abstract void Configure(float speed);

    public abstract void Configure(float speed, float min, float max);

    public abstract void Randomize();
}
