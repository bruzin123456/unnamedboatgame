﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleOscilator : OscilatorBase {

    public SimpleOscilator(float speed = 0.5f)
    {
        this.speed = speed;
        Randomize();
    }

    public SimpleOscilator(float min, float max, float oscilationsPerSecond = 0.5f)
    {
        this.min = min;
        this.max = max;
        Configure(oscilationsPerSecond);
        Randomize();
    }

    private float min = -1;

    private float max = 1;

    private float speed; // oscilations per second

    private float t; //current value seed



    public override void Configure(float oscilationsPersecond)
    {
        this.speed = oscilationsPersecond*Mathf.PI*2;
    }

    public override void Configure(float oscilationsPerSecond, float min, float max)
    {
        this.min = min;
        this.max = max;
        Configure(oscilationsPerSecond);
    }

    public override float GetNextValue()
    {
        return GetNextValue(Time.deltaTime);
    }

    public override float GetNextValue(float deltaTime)
    {
        t += speed * deltaTime;
        float x = Mathf.Sin(t);
        return Mathf.Lerp(min, max, (x+1)/2);
    }

    public override void Randomize()
    {
        t = Random.Range(0, Mathf.PI * 2);
    }

}
