﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicPool<TPoolable> : ObjectPool<TPoolable> where TPoolable : Component, IPoolable
{
    public DynamicPool(GameObject prefab)
    {
        SetPrefab(prefab);
    }

    private GameObject prefab;
    public List<TPoolable> poolList = new List<TPoolable>();

    public override void SetPrefab(GameObject prefab)
    {
        this.prefab = prefab;
    }

    public override bool IsObjectAvailable()
    {
        return poolList.Count > 0;
    }

    public override TPoolable Get()
    {
        TPoolable poolableObject;
        if(IsObjectAvailable())
        {
            poolableObject = poolList[0];
            poolList.RemoveAt(0);
        }
        else
        {
            poolableObject = InitializePoolObject();
        }
        poolableObject.OnPoolRetrieve();
        return poolableObject;
    }

    private TPoolable InitializePoolObject()
    {
        TPoolable poolableObject = GameObject.Instantiate<GameObject>(prefab).GetComponent<TPoolable>();
        poolableObject.gameObject.SetActive(true);
        poolableObject.OnPoolInitialize();
        return poolableObject;
    }

    public override void Recycle(TPoolable poolableObject)
    {
        poolList.Add(poolableObject);
        poolableObject.gameObject.SetActive(false);
        poolableObject.OnPoolRecycle();
    }

    public override void Dispose()
    {
        throw new System.NotImplementedException();
    }

}
