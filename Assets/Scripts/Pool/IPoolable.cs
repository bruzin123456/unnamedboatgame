﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolable
{
    void OnPoolInitialize();  // Poolable object initialized by manager

    void OnPoolRetrieve(); //Poolable object retrieved from pool

    void OnPoolRecycle(); // Poolable object return to pool
}
