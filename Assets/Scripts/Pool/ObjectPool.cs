﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class ObjectPool<TPoolable> where TPoolable : Component, IPoolable
{
     public abstract void SetPrefab(GameObject prefab);

    public abstract bool IsObjectAvailable();

     public abstract TPoolable Get();

     public abstract void Recycle(TPoolable poolableObject);

    public abstract void Dispose();

}
