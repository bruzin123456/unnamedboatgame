﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCameraWorldBounds : MonoBehaviour 
{

    private GameObject minObj;
    private GameObject maxObj;

    // Start is called before the first frame update
    void Start()
    {
        minObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        maxObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
    }

    // Update is called once per frame
    void Update()
    {
        DynamicGameCamera.CameraWorldPositionBounds cameraWorldBounds = DynamicGameCamera.Singleton.GetCameraBounds(0);
        minObj.transform.position = cameraWorldBounds.Min;
        maxObj.transform.position = cameraWorldBounds.Max;
    }
}
