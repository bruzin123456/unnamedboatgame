﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Navigation2D;

public class TestClickInsideNavigationArea : MonoBehaviour {

    public NavigationArea2D navigationAreaTest;

	// Use this for initialization
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
           var clickPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Mathf.Abs(Camera.main.transform.position.z)));
           var clickInside = navigationAreaTest.IsWorldPositionInside(clickPosition);
            if (clickInside)
            {
                Debug.Log("Clicked Inside " + clickPosition);
            }
            else Debug.Log("Not Inside " + clickPosition);
        }
		
	}
}
