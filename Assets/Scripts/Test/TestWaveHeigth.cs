﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestWaveHeigth : MonoBehaviour {

    public WaveGenerator waveGenerator;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float posY = waveGenerator.GetWaveHeigthAtPosition(transform.position.x);
        transform.position = new Vector3(transform.position.x, posY, transform.position.z);
		
	}
}
