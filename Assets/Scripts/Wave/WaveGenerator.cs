﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveGenerator : MonoBehaviour {


    [SerializeField] private GameObject wavePrefab;
    [SerializeField] [Range(0, 3)] private float speed = 0.2f;
    [SerializeField] [Range(0, 10)] private float intensity = 0.1f;
    [SerializeField] [Range(0, 1)] private float wavesScale = 1;
    [SerializeField] [Range(0, 1)] private float waveSize = 1;

    private float wOffset; // generator time position
    private float seed; // wave generator random seed
    private List<WaveParticle> activeWavesParticles = new List<WaveParticle>();
    private DynamicPool<WaveParticle> waveParticlesPool;
    private DynamicGameCamera.CameraWorldPositionBounds cameraWorldBounds;
    private float waveParticleSpriteSize = 1;
    private float waveParticlesScale;


    private float minX { get { return cameraWorldBounds.Left; } }

    private float maxX { get { return cameraWorldBounds.Right; } }



    private void Awake()
    {
        waveParticlesPool = new DynamicPool<WaveParticle>(wavePrefab);
        seed = Random.Range(-10000, 10000); // Initialize wave random seed     
        waveParticlesScale = waveSize / waveParticleSpriteSize;
    }

    private WaveParticle getWaveParticle()
    {
        WaveParticle wParticle = waveParticlesPool.Get();
        wParticle.transform.SetParent(transform);
        wParticle.transform.localScale = new Vector3(waveParticlesScale, 1, 1);
        wParticle.gameObject.SetActive(true);
        activeWavesParticles.Add(wParticle);
        return wParticle;
    }
   
	// Update is called once per frame
	void Update () {
        cameraWorldBounds = DynamicGameCamera.Singleton.GetCameraBounds(transform.position.z);
        int nParticles = Mathf.CeilToInt(cameraWorldBounds.Length / waveSize);

        // Proccess wave particles
        for (int i = 0;  i < nParticles; i++)
        {
            WaveParticle wParticle = null;
            if(i < activeWavesParticles.Count)
            {
                wParticle = activeWavesParticles[i];
            }
            else
            {
                wParticle = getWaveParticle();
            }
            float wX = minX + (waveSize/2) + (waveSize * i);
            float r = (Mathf.PerlinNoise(wOffset + (wX*wavesScale), seed) * 2) - 1;
            float wY = r * intensity;
            wParticle.transform.position = new Vector3(wX, wY, transform.position.z);
        }
        //Move unused particles back to pool
        if(activeWavesParticles.Count > nParticles)
        {
            for(int j = nParticles; j < activeWavesParticles.Count; j++)
            {
                waveParticlesPool.Recycle(activeWavesParticles[j]);
            }
            activeWavesParticles.RemoveRange(nParticles, activeWavesParticles.Count - nParticles);
        }
       //
        wOffset += Time.deltaTime * speed;
	}

    public float GetWaveHeigthAtPosition(float posX)
    {
        if (posX <= minX)
        {
            return activeWavesParticles[0].transform.position.y;
        }
        else if (posX >= maxX)
        {
            return activeWavesParticles[activeWavesParticles.Count - 1].transform.position.y;

        }
        else
        {
            float deltaPosX = posX - minX;
            int waveIndex = Mathf.CeilToInt(deltaPosX / waveSize);
            return activeWavesParticles[waveIndex - 1].transform.position.y;
        }

    }

    public float getWaveAngleAtPosition(float posX)
    {
        if (posX <= minX)
        {
            return AuxiliarMethods.AngleBetweenPointsDegrees(activeWavesParticles[0].transform.position, activeWavesParticles[1].transform.position);
        }
        else if (posX >= maxX)
        {
            return AuxiliarMethods.AngleBetweenPointsDegrees(activeWavesParticles[activeWavesParticles.Count - 1].transform.position, activeWavesParticles[activeWavesParticles.Count - 2].transform.position);
        }
        else
        {
            float deltaPosX = posX - minX;
            int waveIndex = Mathf.CeilToInt(deltaPosX / waveSize);
            return AuxiliarMethods.AngleBetweenPointsDegrees(activeWavesParticles[waveIndex - 1].transform.position, activeWavesParticles[waveIndex].transform.position) ;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(new Vector3(-100000, transform.position.y, transform.position.z), new Vector3(1000000, transform.position.y, transform.position.z));
    }




}